## https://icecream95.gitlab.io/life

This is a simple JS implementation of Conway's Game of Life, which features a
number of different modes for colouring the cells, and randomly flips a cell
every frame to keep things changing.
